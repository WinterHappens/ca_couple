﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Couple
{
    class Program
    {
        static void Main(string[] args)
        {
            char[] originalPairs;
            char[] findPairs;
            int countElems;
            int countPairs;
            bool hasStars;
            char STAR_SYMBOL = '*';
            char startSymbol = 'A';
            char finishSymbol;
            Random rnd = new Random();
            int index1, index2;

            do
            {
                Console.Write("Input countElems: ");
                countElems = int.Parse(Console.ReadLine());
            } while (countElems > 52 || countElems < 4 || countElems % 2 != 0);

            countPairs = countElems / 2;

            finishSymbol = Convert.ToChar(Convert.ToInt32(startSymbol) + countPairs - 1);

            originalPairs = new char[countElems];
            findPairs = new char[countElems];

            for (int i = 0; i < countElems; i++)
            {
                findPairs[i] = STAR_SYMBOL;
                originalPairs[i] = STAR_SYMBOL;
            }

            for (char symb = startSymbol; symb <= finishSymbol; symb++)
            {
                int index;
                do
                {
                    index = rnd.Next(0, countElems);
                } while (originalPairs[index] != STAR_SYMBOL);
                originalPairs[index] = symb;

                do
                {
                    index = rnd.Next(0, countElems);
                } while (originalPairs[index] != STAR_SYMBOL);
                originalPairs[index] = symb;
            }

            /*for (int i = 0; i < countElems; i++)
            {
                Console.Write(originalPairs[i] + " ");
            }*/


            do
            {
                Console.Clear();
                for (int i = 0; i < countElems; i++)
                {
                    Console.Write(findPairs[i] + " ");
                }
                Console.WriteLine();

                do
                {
                    Console.Write("Input index1: ");
                    index1 = int.Parse(Console.ReadLine());
                } while (index1 < 0 || index1 > countElems - 1 || findPairs[index1] != STAR_SYMBOL);

                findPairs[index1] = originalPairs[index1];

                Console.Clear();
                for (int i = 0; i < countElems; i++)
                {
                    Console.Write(findPairs[i] + " ");
                }
                Console.WriteLine();

                do
                {
                    Console.Write("Input index2: ");
                    index2 = int.Parse(Console.ReadLine());
                } while (index2 < 0 || index2 > countElems - 1 || findPairs[index2] != STAR_SYMBOL);

                findPairs[index2] = originalPairs[index2];

                Console.Clear();
                for (int i = 0; i < countElems; i++)
                {
                    Console.Write(findPairs[i] + " ");
                }
                Console.WriteLine();
                Console.ReadKey();

                if (findPairs[index1] != findPairs[index2])
                {
                    findPairs[index1] = STAR_SYMBOL;
                    findPairs[index2] = STAR_SYMBOL;
                }
                

                hasStars = false;
                for (int i = 0; i < countElems; i++)
                {
                    if (findPairs[i] == STAR_SYMBOL)
                    {
                        hasStars = true;
                        break;
                    }
                }
            } while (hasStars == true);

            Console.Write("Victory!");

            Console.ReadKey();
        }
    }
}
